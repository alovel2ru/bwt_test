BWT task
=======================

Для того что бы развернуть проект - нужно:

- Развернуть базу, бекапы которой лежат по адресу "application\database\backup" (MySql 5.6)
- В файле application\core\config.php - установить пароль от БД.
- В файле application\database\SQL\Mysqli.php - установить данные БД.
- Скачать и установить Composer
- Скачать и установить Guzzle(composer.phar лежит в главной дир., или напрямую)
- Запустить проект. (Apache-2.4, PHP 5.6)