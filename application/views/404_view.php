
<!DOCTYPE html>
<html>
<head>
	<title>OLP Study - 404</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/css/404.css">
</head>
<body>

<div class="error">
	<big>404</big>
	<p>Ooooops, this page not found!</p>
	<small><a href="//<?=$_SERVER['HTTP_HOST'];?>">Click here</a> to go home</small>
</div>

</body>
</html>
