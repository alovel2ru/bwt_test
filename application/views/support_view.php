<div class="container">
	<div class="row">
		<div class="col-sm-12 col-lg-12 col-xs-12">
			<h2>Создание тикета:</h2>
				<form class="form-horizontal">
					<div class="form-group">
						<label class="control-label col-xs-3" for="name">Ваше имя:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="name" placeholder="Тема">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="email">Ваша почта:</label>
						<div class="col-xs-9">
							<input type="email" class="form-control" id="email" placeholder="Ваша почта">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="message">Сообщение:</label>
						<div class="col-xs-9">
							<textarea type="text" class="form-control" id="message" placeholder="Сообщение" rows="6"></textarea>
						</div>
					<div class="form-group mg">
						<div class="col-xs-offset-3 col-xs-9">
							<button class="btn btn-outline-success my-2 my-sm-0" type="submit"  id="send">Отправить</button>
							<input type="reset" class="btn btn-default" value="Очистить форму">
						</div>
					</div>
				</form>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-lg-12 col-xs-12">
			<h2>Имеющиеся тикеты:</h2>
		</div>
		<div class="col-sm-12 col-lg-12 col-xs-12">
			<?foreach ($data as $ticket) {?>
				<div class="ticket bot">
					<div class="row">			
					<div class="col-sm-3 col-lg-3 col-xs-3"><h5>Имя:</h5></div>
					<div class="col-sm-9 col-lg-9 col-xs-9"><?=$ticket['name']?></div>
					</div>
					<div class="row">
						<div class="col-sm-3 col-lg-3 col-xs-3"><h5>Почта:</h5></div>
						<div class="col-sm-9 col-lg-9 col-xs-9"><?=$ticket['email']?></div>
					</div>
					<div class="row">
						<div class="col-sm-3 col-lg-3 col-xs-3"><h5>Сообщение:</h5></div>
						<div class="col-sm-9 col-lg-9 col-xs-9"><?=$ticket['message']?></div>
					</div>
				</div>
			<?}?>
		</div>
	</div>
</div>


<script type="text/javascript">
	
$(document).ready(function(){
	$("#send").click(function(){
		var name = $("#name").val();
		var email = $("#email").val();
		var message = $("#message").val();

		$.ajax({
			url: "/api/ticket",
			type: "POST",
			data: {name, email, message},
			success: function(response) {
				if (response.success) {
					document.location.reload();
				}
				else {
					alert(`SERVER ERROR ${response.error}`);
				}
			}
		});	
	});
});

</script>