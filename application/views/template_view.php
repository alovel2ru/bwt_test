<!DOCTYPE html>
<html>
<head>
	<title>BWT</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
	

	<script type="text/javascript" src="/js/main.js"></script>
</head>
<body>

	<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
		<a class="navbar-brand" href="#">BWT</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarCollapse">
				<? if(Model::isLogined()) {?>
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
						<a class="nav-link" href="weather">Погода</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="support">Обратная связь</a>
						</li>
					</ul>
					<div class="user">Вы вошли как: <?=$_SESSION['user']['first_name']?> <?=$_SESSION['user']['last_name']?></div>	
					<button class="btn btn-outline-success my-2 my-sm-0 btnwhite mgleft" type="submit"><a href="signout" id="signout">Выйти</a></button>
				<?}
				else {?>
					<ul class="navbar-nav mr-auto-left">
						<li class="nav-item">
							<div class="form-group zeromarg">
								<input type="text" class="form-control" id="login" placeholder="Email" autofocus="">
							</div>
						</li>
						<li class="nav-item">
							<div class="form-group zeromarg">
							<input type="password" class="form-control" id="password" placeholder="Password">
							</div>
						</li>
					</ul>	
					<button class="btn btn-outline-success my-2 my-sm-0 btnwhite mgleft" type="submit"  id="signin">Войти</button>
				<?}?>
		</div>
	</nav>

<?
	// LOAD CUSTOM VIEW
	include 'application/views/'.$content_view; 
?>

<script>

$(document).ready(function(){

	$("#signin").click(function(){
		var email = $("#login").val();
		var password = $("#password").val();

		$.ajax({
			url: "/api/signin",
			type: "POST",
			data: {email, password},
			success: function(res) {
				console.log(res);
				if (res.success != undefined) {
					console.log(`Login: ${res.success}`);
					if (res.success == true) {
						document.location.reload();
					}
					else {
						alert(`Error: ${res.error}`, "Login error!", false);
					}
				}
				else alert("Server Error! Check console", "Error", false);
			}
		});

		return false;
	});	
	
});
</script>
</body>
</html>
