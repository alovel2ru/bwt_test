<div class="container">
	<h2>Регистрация</h2>
	<form class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-xs-3" for="last_name">Фамилия:</label>
			<div class="col-xs-4">
				<input type="text" class="form-control" id="last_name" placeholder="Введите фамилию">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-xs-3" for="first_name">Имя:</label>
			<div class="col-xs-4">
				<input type="text" class="form-control" id="first_name" placeholder="Введите имя">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-xs-3">Дата рождения:</label>
			<div class="col-xs-4">
				<input class="birthday" type="date" id="birthday">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-xs-3" for="email">Email:</label>
			<div class="col-xs-4">
				<input type="email" class="form-control" id="email" placeholder="Email">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-xs-3" for="pwd">Пароль:</label>
			<div class="col-xs-4">
				<input type="password" class="form-control" id="pwd" placeholder="Введите пароль">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-xs-3">Пол:</label>
			<div class="col-xs-2">
				<label class="radio-inline">
					<input type="radio" name="gender" value="man"> Мужской
				</label>
			</div>
			<div class="col-xs-2">
				<label class="radio-inline">
					<input type="radio" name="gender" value="woman"> Женский
				</label>
			</div>
		</div>
		<br />
		<div class="form-group">
			<div class="col-xs-offset-3 col-xs-4">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit"  id="signup" >Зарегистрироваться</button>
				<input type="reset" class="btn btn-default" value="Очистить форму">
			</div>
		</div>
	</form>
</div>

<script>

$("#signup").click(function(){
	var first_name = $("#first_name").val();
	var last_name  = $("#last_name").val();
	var email      = $("#email").val();
	var password   = $("#pwd").val();
	var birthday   = $("#birthday").val();
	var gender     = $('input[name=gender]:checked', '.form-horizontal').val();

	$.ajax({
		url: "/api/signup",
		type: "POST",
		data: {first_name, last_name, email, password, birthday, gender},
		success: function(res) {
			console.log(res);
			if (res.success != undefined) {
				console.log(`Signup: ${res.success}`);
				if (res.success == true) {
					alert('Successfully registered');
				}
				else {
					alert(`Error: ${res.error}`);
				}
			}
			else alert("Server Error! Check console");
		}
	});
	return false;
});	

</script>