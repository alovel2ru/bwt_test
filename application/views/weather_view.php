<?
//var_dump($data);
?>
<div class="container">
	<?foreach ($data as $weather) {?>
		<div class="row align bot">
			<div class="col-sm-2 col-lg-2 col-xs-2 br-right">
				<div class="date"><?=$weather['date']?></div>
			</div>
			<div class="col-sm-10 col-lg-10 col-xs-10 max">
				<div class="row max">
					<div class="col-sm-5 col-lg-5 col-xs-5">
						<div class="day mg"><h3><?=$weather['month']?>,</h3><h2><?=$weather['day']?></h2></div>
					</div>
					<div class="col-sm-2 col-lg-2 col-xs-2 max">
						<div class="image"><img src="<?=$weather['image']?>" alt="weather" class="rounded mx-auto d-block max"></div>
					</div>
					<div class="col-sm-3 col-lg-3 col-xs-3 mg-left">
						<div class="temp mg">
							<div class="min"><?=$weather['min']?></div>
							<hr>
							<div class="max"><?=$weather['max']?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?}?>
</div>