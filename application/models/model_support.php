<?php
class Model_Support extends Model {
	public function getTicket() {
		$bd = DB_Mysqli::getInstance(); // Connect BD
		$data = $bd->listAll('ticket');
		return $data;
	}
	
	public function addTicket() {
		$name = $this->clearHtml($_POST['name']);
		$email = $this->clearHtml($_POST['email']);
		$message = $this->clearHtml($_POST['message']);

		$bd = DB_Mysqli::getInstance(); // Connect BD
		
		$response = $bd->insertAssocOne('ticket', ['name' => $name, 'email' => $email, 'message'   => $message]);
		
		if (!$response['SQL_PARSE']) {
			$returnArray['success'] = true;
		}
		else {
			$returnArray['success'] = false;
			$returnArray['error'] = $response['SQL_PARSE'];
		}
		return $returnArray;
	}
}
