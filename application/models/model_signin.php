<?
class Model_Signin extends Model {
	public function login() {
		if(!$this->isClearPost() && $this->checkInput()) {
			
			$bd = DB_Mysqli::getInstance(); // connect

			$response = $bd->listByKeyOne('users', $_POST['email'], 'email');
			
			if (!$response['SQL_PARSE'] && count($response)) {
				$data = $this->checkPassword($_POST['password'], $response['password']);
				if($data) {
					$this->setSession('user', $response);
					$returnArray['success'] = true;
					$returnArray['res'] = $response;
				}
				else {
					$returnArray['success'] = false;
					$returnArray['error'] = 'Wrong password';
				}
			}
			else {
				$returnArray['success'] = false;
				$returnArray['error'] = 'Can\'t find user';
			}
		}
		else {
			$returnArray['success'] = false;
			$returnArray['error'] = 'Wrong or clear input';
		}
		return $returnArray;
	}

	public function checkInput() {
		$rules = array( 'password' => (preg_match("/^[-a-zA-Z]{6,32}+$/u", $_POST['email']) ? true : false),
						'email'    => (preg_match(" /^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})+$/", $_POST['email']) ? true : false),
		);

		foreach ($rules as $field => $value) {
			if ($value) {
				$checked[$field] = $this->clearHtml($_POST[$field]);
			}
		}
		return $checked;
	}
	public function checkPassword($password, $hash){
		if (hash_equals($hash, crypt($password, $hash))) {
			return true;
		}
		else {
			return false;
		}
	}
}


