<?php
class Model_Signout extends Model {
	function logout() {
		session_destroy();
		self::redirectTo('signup');
	}
}
