<?php
class Model_Weather extends Model {
	public function getWeather() {
		// Create guzzle instance
		$client = new GuzzleHttp\Client();
		// Send request.
		$response = $client->request('GET', urldecode(config::WEATHER_URL));
		// Parse result
		if(preg_match_all('#<div class="main(.+?)"></div>#is', $response->getBody(), $result_html)){
			$weatherClear = strip_tags($result_html[0][0], '<img>');
			$weatherArray = explode('&nbsp;', $weatherClear);
			array_pop($weatherArray);
			
			foreach ($weatherArray as $key => $value) {
				// find
				$handler = explode(' ', trim($value));
				preg_match('#//sinst.fwdcdn.com/img/(.+?).gif#is', $value, $handlerImage);
				preg_match('#мин.(.+?)deg#is', $value, $handlerMin);
				preg_match('#макс.(.+?)deg#is', $value, $handlerMax);
				// record
				$answer[$key]['day'] = $handler[0];
				$answer[$key]['date'] = $handler[1];
				$answer[$key]['month'] = $handler[2];
				$answer[$key]['image'] = $handlerImage[0];
				$answer[$key]['min'] = $handlerMin[0];
				$answer[$key]['max'] = $handlerMax[0];
			}

			return $answer;
		}
		else {
			return false;
		}
	}
}
