<?php
class Model_Signup extends Model {
	public function signup() {
		if(!$this->isClearPost()) {
			
			$bd = DB_Mysqli::getInstance(); // connect

			$arrayInsert = $this->checkInput();

			if(!$arrayInsert) {
				$returnArray['success'] = false;
				$returnArray['error'] = 'Required fields wrong/not filled';
				return $returnArray;
			}

			$response = $bd->insertAssocOne('users', $arrayInsert);
			
			if (!$response['SQL_PARSE']) {
				$returnArray['success'] = true;
			}
			else {
				$returnArray['success'] = false;
				$returnArray['error'] = 'Can\'t create user';
			}
		}
		else {
			$returnArray['success'] = false;
			$returnArray['error'] = 'Empty field!';
		}
		return $returnArray;
	}

	public function checkInput() {
		$rules =   ['first_name' => (preg_match("/^[-a-zA-ZА-Яа-яёЁЇїІіЄєҐґ]{3,32}+$/u", $_POST['first_name']) ? $_POST['first_name'] : false),
					'last_name'  => (preg_match("/^[-a-zA-ZА-Яа-яёЁЇїІіЄєҐґ]{3,32}+$/u", $_POST['last_name']) ?  $_POST['last_name'] : false),
					'password'   => (preg_match("/^[-a-zA-Z0-9]{6,32}+$/u", $_POST['password']) ? crypt($_POST['password']) : false),
					'email'      => (preg_match(" /^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})+$/", $_POST['email']) ?  $_POST['email'] : false),
					'birthday'   => (preg_match("/^[-0-9]{6,10}+$/", $_POST['birthday']) ? $_POST['birthday'] : false),
					'gender'     => ($_POST['gender'] == 'man' || $_POST['gender'] == 'woman' ? $_POST['gender'] : false)
		];
		foreach ($rules as $field => $value) {
			if ($value) {
				$checked[$field] = $rules[$field];
			}
		}
		if($checked['first_name'] && $checked['last_name'] && $checked['password'] && $checked['email']) {
			return $checked;
		}
		else {
			return false;
		}
		
	}
}
?>