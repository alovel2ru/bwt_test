<?php
class View
{
	public $template_view='template.php';
	
	function generate($content_view, $template_view = '', $data = null, $error = '') {

		if(is_array($data)) {
			// преобразуем элементы массива в переменные
			extract($data);
		}
		
		($template_view) ? include 'application/views/'.$template_view : include 'application/views/'.$content_view; ;
	}
}