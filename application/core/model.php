<?
class Model {

	public static function isLogined() {
		if($_SESSION['user']) {
			return true;
		}
		else {
			return false;
		}
	}

	public function clearUserSession(array $name) {
		foreach ($name as $key => $sessName) {
			unset($_SESSION[$sessName]);
		}
		return true;
	}

	public function isClearPost() {
			return $state = ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)) ? true : false; 
	}

	public function redirectTo($path = "") {
		if (!headers_sent()) {
			header("Location: " /*. config::DOMAIN_NAME*/ . $path);
			exit;
		}
	}

	public function setSession($key , array $arrayPut) {
		unset($_SESSION[$key]);
		$_SESSION[$key] = $arrayPut;
	}
	
	public function setMultiSession(array $param) {
		foreach ($param as $key => $value) {
			self::setSession($key, $value);
		}
	}

	public function updateSession($tableName, array $arrayUpdate) {
		foreach ($_SESSION as $keySession => $fld) { 
			if($keySession == $tableName) {
				foreach ($_SESSION[$keySession] as $field => $fieldValue) {
					if(isset($arrayUpdate[$field])) {
						$_SESSION[$keySession][$field] = $arrayUpdate[$field];
					}
					else {
						continue;
					}
				}
			}
			else {
				continue;
			}
		}
	}

	public function concatSession($key, $value) {
		$_SESSION[$key] = array_merge($_SESSION[$key],$value);
	}

	public function setCookies($name, $value, $time, $path) {
		@setcookie($name, $value, $time, $path);
	}

	public function clearHtml($html) {
		return strip_tags(trim($html));
	}	
}
