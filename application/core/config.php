<?php
class config {
	// For routing
	const DEFAULT_CONTROLLER = 'signup';
	const DEFAULT_ACTION = 'index';
	const PATH_MODELS = 'application/models/model_';
	const PATH_CONTROLLERS = 'application/controllers/controller_';
	const PATH_VIEWS = 'application/views/';
	const ACTION_PRE = 'action_';
	const MODEL_PRE = 'Model_';
	const CONTROLLER_PRE = 'Controller_';
	// other
	const DOMAIN_NAME = 'http://bwt/';
	// DB
	const DB_LOCAL_PASSWORD = '';
	const DB_SERVER_PASSWORD = '';
	// Weather
	const WEATHER_URL = 'https://sinoptik.ua/погода-запорожье/10-дней'; 

	public static $db_password;
	
	public static function refreshData() {
		self::$db_password = ($_SERVER['SERVER_ADDR'] == '127.0.0.1' && $_SERVER['SERVER_PORT'] == '80') ? self::DB_LOCAL_PASSWORD : self::DB_SERVER_PASSWORD;
	}
}

?>