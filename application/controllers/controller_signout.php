<?php
class Controller_Signout extends Controller {
	function __construct() {
		$this->model = new Model_Signout();
		$this->view = new View();
	}
	
	function action_index() {
		$data = $this->model->logout();	
	}
}