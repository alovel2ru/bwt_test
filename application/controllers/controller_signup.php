<?php
class Controller_Signup extends Controller {
	function __construct() {
		$this->model = new Model_Signup();
		$this->view = new View();
	}
	function action_index() {
		if ($this->model->isLogined()) {
			$this->model->redirectTo('weather');
		}
		else {
			$this->view->generate('signup_view.php', 'template_view.php');
		}
	}
}
?>