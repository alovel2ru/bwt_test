<?
class Controller_Support extends Controller
{
	function __construct() {
		$this->model = new Model_Support();
		$this->view = new View();
	}

	public function action_index() {
		if ( $this->model->isLogined() ) {
			$data = $this->model->getTicket();
			$this->view->generate('support_view.php', 'template_view.php', $data);
		}
		else {
			$this->model->redirectTo('signup');
		}
	}
}