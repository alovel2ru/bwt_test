<?
class Controller_Api extends Controller {
	function action_signin() {
		include __DIR__. '/../models/model_signin.php';
		$this->model = new Model_Signin;

		$success = $this->model->login();
		$this->json($success);
	}

	function action_signup() {
		include __DIR__. '/../models/model_signup.php';
		$this->model = new Model_Signup;
		
		$success = $this->model->signup();
		$this->json($success);
	}
	function action_ticket() {
		include __DIR__. '/../models/model_support.php';
		$this->model = new Model_Support;
		
		$success = $this->model->addTicket();
		$this->json($success);
	}
}
?>
