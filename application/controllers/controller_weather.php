<?
class Controller_Weather extends Controller
{
	function __construct() {
		$this->model = new Model_Weather();
		$this->view = new View();
	}

	public function action_index() {
		if ( $this->model->isLogined() ) {
			$data = $this->model->getWeather();
			$this->view->generate('weather_view.php', 'template_view.php', $data);
		}
		else {
			$this->model->redirectTo('signup');
		}
	}
}